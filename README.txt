YUI:
-----
http://developer.yahoo.com/yui
http://developer.yahoo.com/yui/treeview/


Installation:
-----
There is a link to a video that shows how to install the module
on the project homepage: http://www.drupal.org/project/yui_treeview

Author:
-----
Jeff Decker <jeff at jeffcd dot com>

Sponsor:
-----
<none />
